package mailfollowup;

import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class App 
{
	public LocalDateTime followupTime(LocalDateTime now, final String emailadress) {
		if(isStringDate(emailadress)){
			now = setDate(now, emailadress);
		}
		else {
			now = addDays(now, emailadress);
			now = addHours(now, emailadress);
			now = addWeeks(now, emailadress);
		}
		return now;
	}

	public LocalDateTime addDays(final LocalDateTime now, final String emailadress) {
		final String timeString = emailadress.split("@")[0];
		long days = 0;
		if (timeString.contains("day")) {
			final int index = timeString.indexOf('d');

			days = findNumber(timeString, index);
		}

		return now.plusDays(days);
	}

	public LocalDateTime addHours(final LocalDateTime now, final String emailadress) {
		final String timeString = emailadress.split("@")[0];
		long hours = 0;
		if (timeString.contains("hour")) {
			final int index = timeString.indexOf('h');

			hours = findNumber(timeString, index);
		}

		return now.plusHours(hours);
	}

	public LocalDateTime addWeeks(final LocalDateTime now, final String emailadress) {
		final String timeString = emailadress.split("@")[0];
		long weeks = 0;
		if (timeString.contains("week")) {
			final int index = timeString.indexOf('w');

			weeks = findNumber(timeString, index);
		}

		return now.plusWeeks(weeks);
	}

	public LocalDateTime setDate(final LocalDateTime now, final String emailadress) {
		final String string = emailadress.split("@")[0];

		final String dateString = string.split("-")[0];
		final String timeString = string.split("-")[1];

		final int year = now.getYear();

		final Long day = findNumber(dateString, dateString.length());
		final Month month = parseMonth(dateString.substring(0, 3));

		final int index;
		Long hour;
		if(isAfternoonTime(timeString)){
			index = timeString.indexOf('p');
			hour = findNumber(timeString, index);
			hour += 12;
		}
		else{
			index = timeString.indexOf('a');
			hour = findNumber(timeString, index);
		}

		return LocalDateTime.of(year, month, day.intValue(), hour.intValue(), 0, 0);
	}

	private boolean isAfternoonTime(String timeString) {
		return timeString.contains("pm");
	}

	public long findNumber(final String timeString, int index) {
		final List<Character> digits = new ArrayList<>();
		boolean isDigit = true;
		while (isDigit && index > 0) {
			--index;
			if (Character.isDigit(timeString.charAt(index))) {
				digits.add(timeString.charAt(index));
			} else {
				isDigit = false;
			}
		}
		Collections.reverse(digits);

		final StringBuilder builder = new StringBuilder(digits.size());
		for (final Character c : digits) {
			builder.append(c);
		}

		return Long.parseLong(builder.toString());
	}

	public boolean isStringDate(final String emailadress) {
		return Character.isLetter(emailadress.charAt(0));
	}

	public Month parseMonth(final String string) {
		
		Month month = null;
		switch(string){
			case "jan": month = Month.JANUARY;
				break;
			case "feb": month = Month.FEBRUARY;
				break;
			case "mar": month = Month.MARCH;
				break;
			case "apr": month = Month.APRIL;
				break;
			case "may": month = Month.MAY;
				break;
			case "jun": month = Month.JUNE;
				break;
			case "jul": month = Month.JULY;
				break;
			case "aug": month = Month.AUGUST;
				break;
			case "sep": month = Month.SEPTEMBER;
				break;
			case "oct": month = Month.OCTOBER;
				break;
			case "nov": month = Month.NOVEMBER;
				break;
			case "dec": month = Month.DECEMBER;
				break;
		}
		return month;
	}
}
