package mailfollowup;

import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.Month;

import org.junit.Test;

public class AppTest 
{
    @Test
    public void followupTimeTest(){

        LocalDateTime now = LocalDateTime.of(2013, Month.FEBRUARY, 4, 10, 30, 0);
        String emailadress = "2weeks1day1hour@followup.cc";
        App app = new App();
        LocalDateTime actual = app.followupTime(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2013, Month.FEBRUARY, 19, 11, 30, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void followupTimeByDateTest(){

        LocalDateTime now = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0);
        String emailadress = "aug15-9am@followup.cc";
        App app = new App();
        LocalDateTime actual = app.followupTime(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2020, Month.AUGUST, 15, 9, 0, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void addDaysTest(){

        LocalDateTime now = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0);
        String emailadress = "7days@followup.cc";
        App app = new App();
        LocalDateTime actual = app.addDays(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2020, Month.JANUARY, 8, 0, 0, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void addHoursTest(){

        LocalDateTime now = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0);
        String emailadress = "12hours@followup.cc";
        App app = new App();
        LocalDateTime actual = app.addHours(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2020, Month.JANUARY, 1, 12, 0, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void addWeeksTest(){

        LocalDateTime now = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0);
        String emailadress = "2weeks@followup.cc";
        App app = new App();
        LocalDateTime actual = app.addWeeks(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2020, Month.JANUARY, 15, 0, 0, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void setDateTest(){

        LocalDateTime now = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0);
        String emailadress = "aug15-9am@followup.cc";
        App app = new App();
        LocalDateTime actual = app.setDate(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2020, Month.AUGUST, 15, 9, 0, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void setDateAfternoonTest(){

        LocalDateTime now = LocalDateTime.of(2020, Month.JANUARY, 1, 0, 0, 0);
        String emailadress = "aug15-9pm@followup.cc";
        App app = new App();
        LocalDateTime actual = app.setDate(now, emailadress);

        LocalDateTime expected = LocalDateTime.of(2020, Month.AUGUST, 15, 21, 0, 0);

        assertTrue(expected.equals(actual));
    }

    @Test
    public void isStringDateTest(){
        
        String emailadress = "aug15-9am@followup.cc";
        App app = new App();
        boolean actual = app.isStringDate(emailadress);

        assertTrue(actual);
    }

    @Test
    public void parseMonthTest(){

        String[] strings = new String[]{
            "jan", "feb", "mar", "apr", "may", "jun", "jul", "aug", "sep", "oct", "nov", "dec"
        };

        assertTrue(strings.length==12);

        App app = new App();
        for(int i=0; i<strings.length; ++i){
            Month actual = app.parseMonth(strings[i]);
            Month expected = Month.JANUARY.plus(i);
            
            assertTrue(expected.equals(actual));
        }
    }
}
